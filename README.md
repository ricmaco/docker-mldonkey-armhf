# ricmaco/mldonkey-armhf

[MLDonkey](http://mldonkey.sourceforge.net/Main_Page) is an open-source,
multi-protocol, peer-to-peer file sharing application that runs as a back-end
server application on many platforms. It can be controlled through a user
interface provided by one of many separate front-ends, including a Web
interface, telnet interface and over a dozen native client programs. (from
[Wikipedia](https://en.wikipedia.org/w/index.php?title=MLDonkey&oldid=875114813))

## Usage

```
docker create \
  --name=mldonkey \
  -p 4080:4080 \
  ...
  -v <path to data>:/config \
  -v <path to download>:/peers \
  ricmaco/mldonkey-armhf
```

#### Tags

+ **1.3.0**: updated base to Focal Fossa.
+ **1.2.0**: updated base to upstream.
+ **1.1.0**: updated base to upstream.
+ **1.0.0**: first release.

## Parameters

The parameters are split into two halves, separated by a colon, the left hand side representing the host and the right the container side.
For example with a port -p external:internal - what this shows is the port mapping from internal to external of the container.
So -p 8080:80 would expose port 80 from inside the container to be accessible from the host's IP on port 8080
http://192.168.x.x:8080 would show you what's running INSIDE the container on port 80.

* `-p 1234` - the port(s)
* `-v /config` - Where MLDonkey stores its config files
* `-v /peers` - Where you want MLDonkey to store downloaded files
* `--net=host` - Optional internal network

It is based on Ubuntu Linux (Focal Fossa) with s6 overlay, for shell access whilst the container is running do `docker exec -it mldonkey /bin/bash`.

## Info

* Shell access whilst the container is running: `docker exec -it mldonkey /bin/bash`
* To monitor the logs of the container in realtime: `docker logs -f mldonkey`

* container version number

`docker inspect -f '{{ index .Config.Labels "build_version" }}' mldonkey`

* image version number

`docker inspect -f '{{ index .Config.Labels "build_version" }}' ricmaco/mldonkey-armhf`

## Versions

+ **10.01.22**: updated base to Focal Fossa
+ **28.04.19**: updated base to upstream.
+ **11.02.19**: Initial version.

## Thanks

This work is based (although quite loosely) on the wonderful work made by
[LinuxServer.io](https://www.linuxserver.io/). Go check them out and contribute
with some beer money!
