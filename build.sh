#!/usr/bin/env sh

set -x
set -e

export BUILD_DATE=`date`
export VERSION=`git describe --tags`

docker build \
    -t ricmaco/mldonkey-armhf:latest \
    --build-arg BUILD_DATE="$BUILD_DATE" \
    --build-arg VERSION="$VERSION" \
    .