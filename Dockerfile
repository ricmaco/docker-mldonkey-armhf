FROM ghcr.io/linuxserver/baseimage-ubuntu:arm32v7-focal

# set version label
ARG BUILD_DATE
ARG VERSION
LABEL build_version="Linuxserver.io version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="Riccardo Macoratti <r.macoratti@gmx.co.uk>"

# environment variables
ARG DEBIAN_FRONTEND="noninteractive"
ENV MLDONKEY_DIR="/config/mldonkey"

RUN echo "**** install runtime packages ****" \
    && apt-get update \
    && apt-get install -y mldonkey-server

RUN echo "**** cleanup ****" \
    && rm -rf \
        /tmp/* \
        /var/lib/apt/lists/* \
        /var/lib/mldonkey \
        /var/tmp/* \
    && mkdir -p \
        /var/lib/mldonkey

# add local files
COPY root/ /

# ports and volumes
EXPOSE 4080
VOLUME /config
